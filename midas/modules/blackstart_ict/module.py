import logging
from typing import Any, Dict, Tuple

from midas.util.upgrade_module import UpgradeModule

LOG = logging.getLogger(__name__)


class BlackstartICTModule(UpgradeModule):

    def __init__(self):
        super().__init__(
            module_name="blackstart_ict",
            default_scope_name="midasmv",
            default_sim_config_name="BlackstartICT",
            default_import_str=(
                "midas.modules.blackstart_ict.simulator:ICTSimulator"
            ),
            default_cmd_str=("%(python)s -m market_agents.simulator %(addr)s"),  # TODO
            log=LOG,
        )

        self.models = {}

    def check_module_params(self, module_params: Dict[str, Any]):
        """Check the module params and provide default values."""
        pass

    def check_sim_params(self, module_params):
        """Check the params for a certain simulator instance."""
        pass

    def start_models(self):
        """Start models of a certain simulator."""
        mod_ctr = 0
        model = "ICTModel"
        mod_key = self.scenario.generate_model_key(self, model.lower(), mod_ctr)
        mod_params = {'scenario_folder_name': self.sim_params['scenario_folder_name'],
                      'bs_batteries': self.sim_params['bs_batteries'],
                      'agent_ied_mapping': self.sim_params['agent_ied_mapping'],
                      'ied_substation_mapping': self.sim_params['ied_substation_mapping'],
                      'bs_mapping': self.sim_params['bs_mapping'],
                      'bc_capable_buses': self.sim_params['bc_capable_buses']}
        self.start_model(mod_key, model, mod_params)
        mod_ctr += 1

    def connect(self):
       pass

    def connect_to_db(self):
        # db_key = self.scenario.find_first_model("store", "database")[0]
        # for agent_key in self.agent_unit_model_map:
        #     self.connect_entities(
        #         agent_key, db_key, ["set_q_schedule", "reactive_power_offer"]
        #     )
        pass