import os
import json
import networkx as nx
import random
import copy


class ICTModel:
    """A model to handle a networkX graph for the ICT system."""

    def __init__(self, scenario_folder_name, bs_batteries, bs_mapping, bc_capable_buses, agent_ied_mapping,
                 ied_substation_mapping):
        folder_name = scenario_folder_name
        bs_number_scenario = bs_mapping
        self.bs_batteries = bs_batteries
        self.bc_capable_buses = bc_capable_buses

        # load files
        this_folder = os.path.dirname(os.path.abspath(__file__))
        agent_mapping_file = os.path.join(this_folder, 'ict_data', folder_name, f'{agent_ied_mapping}.json')
        substation_mapping_file = os.path.join(this_folder, 'ict_data', folder_name,
                                               f'{ied_substation_mapping}.json')
        bs_substation_mapping_file = os.path.join(this_folder, 'ict_data', folder_name, bs_number_scenario + '.json')
        delays_file = os.path.join(this_folder, 'ict_data', folder_name, 'delays.json')

        agent_mapping = open(agent_mapping_file)
        self.agent_mapping = json.load(agent_mapping)

        substation_mapping = open(substation_mapping_file)
        self.substation_mapping = json.load(substation_mapping)

        # print(self.agent_mapping)
        # print(self.substation_mapping)

        bs_substation_mapping = open(bs_substation_mapping_file)
        self.bs_substation_mapping = json.load(bs_substation_mapping)

        delays = open(delays_file)
        self.delays = json.load(delays)

        # random parameters
        self.bs_bus_connection = {}
        self.bs_battery_mapping = {}

        self.initial_ict_graph = self.create_ict_graph()

        # parameters for other simulators
        self.current_ict_graph = None
        """~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~"""
        self.optimal_ict_graph = None
        """~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~"""

    def create_ict_graph(self):
        ict_graph = nx.Graph()
        # at the start of restoration, all BS-BS links are in severely degraded state and all others in slightly

        # create substations and IEDs and links
        for substation, ieds in self.substation_mapping['ied_substation'].items():
            ict_graph.add_node(substation)
            for ied in ieds:
                ict_graph.add_node(ied)
                delay = self.delays['staticDelays']['SubStation-IED']
                ict_graph.add_edge(substation, ied, delay=delay)

        # create base stations, links and bus connections
        for base_station, substations in self.bs_substation_mapping['Basestation_Substation'].items():
            ict_graph.add_node(base_station)
            for substation in substations:
                delay = self.delays['staticDelays']['BaseStation-SubStation']
                ict_graph.add_edge(base_station, substation, delay=delay)

            # create bs_bus connections
            bus = self.bs_substation_mapping['Basestation_Bus'][base_station]
            self.bs_bus_connection[base_station] = bus

        # create bs_bs links
        for connection, nodes in self.bs_substation_mapping['Basestation_Basestation'].items():
            delay = self.delays['staticDelays']['BaseStation-BaseStation']
            if not ict_graph.has_edge(nodes[0], nodes[1]):
                ict_graph.add_edge(nodes[0], nodes[1], delay=delay)

        # connect agents to IEDs
        for agent, ied in self.agent_mapping['agent_ied'].items():
            delay = 0
            ict_graph.add_node(agent)
            ict_graph.add_edge(agent, ied, delay=delay)

        # place random batteries
        number_of_batteries = round(len(self.bs_substation_mapping['Basestation_Substation']) * self.bs_batteries)
        number_of_bc_buses = len(self.bc_capable_buses)
        bs_with_batteries = []
        list_of_bs = list(self.bs_substation_mapping['Basestation_Substation'])

        if self.bs_batteries == 1.0:
            bs_with_batteries = list_of_bs

        else:
            # first find bs at bc capable buses
            for bus in self.bc_capable_buses:
                for bs, bus_number in self.bs_substation_mapping['Basestation_Bus'].items():
                    if bus_number == bus:
                        if bs not in bs_with_batteries:
                            bs_with_batteries.append(bs)
                        if bs in list_of_bs:
                            list_of_bs.remove(bs)

            # then place the rest of the batteries random at other buses (if there are any batteries left)
            if number_of_batteries-number_of_bc_buses > 0:
                random_bs = random.sample(list_of_bs, number_of_batteries - number_of_bc_buses)
                bs_with_batteries.extend(random_bs)

        for base_station in self.bs_substation_mapping['Basestation_Substation'].keys():
            if base_station in list(set(bs_with_batteries)):
                self.bs_battery_mapping[base_station] = True
            else:
                self.bs_battery_mapping[base_station] = False

        return ict_graph

    def step(self, available_buses):
        """Update the current ICT graph with available buses

        """
        new_ict_graph = copy.deepcopy(self.initial_ict_graph)

        # create new graph
        for base_station, bus in self.bs_bus_connection.items():
            if int(bus) not in available_buses:
                if not self.bs_battery_mapping[base_station]:
                    new_ict_graph.remove_node(base_station)

        self.current_ict_graph = new_ict_graph