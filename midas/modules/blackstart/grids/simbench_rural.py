import pandapower as pp
import simbench as sb
from pandapower.plotting.plotly import simple_plotly


def build_grid():
    """Create simbench rural mv grid with additional lines to form connected islands"""

    # load basic simbench grid
    grid = sb.get_simbench_net("1-MV-rural--2-no-sw")

    # add four more tie-lines to connect different grid parts
    pp.create_line(grid, 42, 70, name="MV1.101 loop_line 7", length_km=6.2,
                   std_type="NA2XS2Y 1x70 RM/25 12/20 kV", subnet="MV1.101_Loop_Line", max_loading_percent=100)
    pp.create_line(grid, 13, 39, name="MV1.101 loop_line 8", length_km=5.4,
                   std_type="NA2XS2Y 1x70 RM/25 12/20 kV", subnet="MV1.101_Loop_Line", max_loading_percent=100)
    pp.create_line(grid, 72, 90, name="MV1.101 loop_line 9", length_km=4.8,
                   std_type="NA2XS2Y 1x70 RM/25 12/20 kV", subnet="MV1.101_Loop_Line", max_loading_percent=100)
    pp.create_line(grid, 24, 29, name="MV1.101 loop_line 10", length_km=6.0,
                   std_type="NA2XS2Y 1x70 RM/25 12/20 kV", subnet="MV1.101_Loop_Line", max_loading_percent=100)

    # remove existing switches
    for s_idx, switch in grid.switch.iterrows():
        grid.switch.drop(s_idx, inplace=True)

    # create bus-bus switches on all the lines. Initially all switches are closed except loop line switches
    for l_idx, line in grid.line.iterrows():
        bus_a = line["from_bus"]
        bus_b = line["to_bus"]
        switch_name = "switch-" + str(l_idx)
        if "loop_line" in line["name"]:
            initial_switch_state = False
        else:
            initial_switch_state = True
        pp.create_switch(grid, bus=bus_a, element=bus_b, et="b", type="LBS", name=switch_name,
                         closed=initial_switch_state)

    # simple_plotly(grid)

    # all_lines = []
    # for line_idx, line in grid.line.iterrows():
    #     all_lines.append((line['from_bus'], line['to_bus']))
    #
    # print(all_lines)

    return grid

build_grid()
