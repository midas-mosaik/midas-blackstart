import pandapower as pp
import simbench as sb
from pandapower.plotting.plotly import simple_plotly


def build_grid():
    """Create simbench urban mv grid with additional lines to form connected islands"""

    # load basic simbench grid
    grid = sb.get_simbench_net("1-MV-urban--2-no-sw")

    # add two more tie-lines to connect different grid parts
    pp.create_line(grid, 84, 100, name="MV1.101 loop_line 12", length_km=1.2, std_type="NA2XS2Y 1x240 RM/25 6/10 kV",
                   subnet="MV3.101_Loop_Line", max_loading_percent=100)
    pp.create_line(grid, 31, 50, name="MV1.101 loop_line 13", length_km=0.8, std_type="NA2XS2Y 1x240 RM/25 6/10 kV",
                   subnet="MV3.101_Loop_Line", max_loading_percent=100)

    # remove all lines to bus 8 and reconnect them to bus 136
    new_line_idx = 1
    for l_idx, line in grid.line.iterrows():
        bus_a = line["from_bus"]
        bus_b = line["to_bus"]
        if bus_a == 8:
            grid.line.drop(l_idx, inplace=True)
            pp.create_line(grid, bus_b, 136, name=f"MV1.101 new_line {new_line_idx}", length_km=1.2,
                           std_type="NA2XS2Y 1x240 RM/25 6/10 kV", subnet="MV3.101_Loop_Line", max_loading_percent=100)
        elif bus_b == 8:
            grid.line.drop(l_idx, inplace=True)
            pp.create_line(grid, bus_a, 136, name=f"MV1.101 new_line {new_line_idx}", length_km=1.2,
                           std_type="NA2XS2Y 1x240 RM/25 6/10 kV", subnet="MV3.101_Loop_Line", max_loading_percent=100)

    # add two more tie-lines to connect different grid parts
    pp.create_line(grid, 14, 119, name="MV1.101 loop_line 14", length_km=0.54, std_type="NA2XS2Y 1x240 RM/25 6/10 kV",
                   subnet="MV3.101_Loop_Line", max_loading_percent=100)
    pp.create_line(grid, 31, 140, name="MV1.101 loop_line 15", length_km=0.54, std_type="NA2XS2Y 1x240 RM/25 6/10 kV",
                   subnet="MV3.101_Loop_Line", max_loading_percent=100)

    # remove existing switches
    for s_idx, switch in grid.switch.iterrows():
        grid.switch.drop(s_idx, inplace=True)

    # create bus-bus switches on all the lines. Initially all switches are closed except loop line switches
    for l_idx, line in grid.line.iterrows():
        bus_a = line["from_bus"]
        bus_b = line["to_bus"]
        switch_name = "switch-" + str(l_idx)
        if "loop_line" in line["name"]:
            initial_switch_state = False
        else:
            initial_switch_state = True
        pp.create_switch(grid, bus=bus_a, element=bus_b, et="b", type="LBS", name=switch_name,
                         closed=initial_switch_state)

    # simple_plotly(grid)

    # switch_number = 0

    # all_overlay_links = []
    # for l_idx, line in grid.line.iterrows():
    #     bus_a = line["from_bus"]
    #     bus_b = line["to_bus"]
    #     agent_a = None
    #     agent_b = None
    #     for agent, ied in agent_bus.items():
    #         items = ied.split('_')
    #         bus_number = items[2]
    #         if bus_number == str(bus_a):
    #             agent_a = agent
    #         elif bus_number == str(bus_b):
    #             agent_b = agent

        # if agent_a and agent_b:
        #     string4 = f'"switch-{switch_number}": {{"other_bus": {bus_a}, "other_bus": {bus_b}, access: True}}'
        #     print(string4)
        #     switch_number += 1

    #     if agent_a and agent_b:
    #         overlay_link = [agent_a, agent_b]
    #         all_overlay_links.append(overlay_link)
    #
    # print(all_overlay_links)

    # all_lines = []
    # for line_idx, line in grid.line.iterrows():
    #     all_lines.append((line['from_bus'], line['to_bus']))
    #
    # print(all_lines)

    return grid

build_grid()